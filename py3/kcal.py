#!/bin/python3

PERCENT=100.0
KCAL = 1900 # calories max. idéal /jour, activité modérée pour H/F

# Calculs caloriques
# @args
#   kcal : quantité calorique du produit pour 100g (ex: 118 kcal)
#   g : poids en gramme du produit (ex: 200g)
#   p : quantité consommé en pourcentage (ex: 50%)
def getKcal (kcal, g, p):
    return (kcal/100.0) * g * (p/PERCENT)

# Calculs sur menu
# @args: menu, ex [[ kcal, g, p ]]
def getKcalMenu (menu):
    # Calcul du nombre de calories ingérés
    sum = 0
    for item in menu: sum += getKcal(item[0], item[1], item[2])
    if sum < KCAL:
        print('Total %s kcal (%s%%)' % (sum, round(PERCENT/KCAL*sum, 2)))
        print('RESTANT %s kcal' % round(KCAL-sum, 2))
    elif sum == KCAL:
        print('Total %s kcal' % sum)
        print('IDEAL')
    else:
        print('Total %s kcal (+%s%%)' % (sum, round(PERCENT/KCAL*sum-100, 2)))
        print('EXCES %s kcal.' % round(sum-KCAL, 2))

def main ():
    # Liste des consommations sur une journée
    menu = [
        # MATIN
        # -- rien --
        # MIDI
        [ 319, 180, 50 ],
        [ 540, 200, 25 ],
        # SOIR
    ]
    # Traiter
    getKcalMenu(menu)

main()
