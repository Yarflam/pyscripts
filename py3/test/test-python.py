#!/bin/python3

a = 4
b = 50
c = 6748
m = 1

imax = 1000
i = imax
while i > 0:
    a = (a * b) % c
    if a > m:
        m = a
    i -= 1

print("Max: %s, Gap: %s, Time: %s" % (str(m), str(c-m), str(imax)))
