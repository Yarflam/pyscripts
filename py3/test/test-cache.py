#!/bin/python3
#from functools import cache
import time
import sys

sys.setrecursionlimit(5000)

def opti(fct):
    keys = []
    values = []
    def run(*args):
        if args not in keys:
            values.append(fct(*args))
            keys.append(args)
        return values[keys.index(args)]
    return run

#@cache
def fibo(n):
    if n <= 2: return 1;
    return fibo(n-1) + fibo(n-2)

fibo = opti(fibo)

ts = time.time()
print(fibo(1000))
print(time.time() - ts)
