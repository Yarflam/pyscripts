#!/usr/bin/python3
from datetime import datetime
import math

#############
#   TOOLS   #
#############

# Format Right Ascension -> Angle 360°
def raToAngle (ra):
    return ra[0] * (360/24) + ra[1] * (360/(24*60)) + ra[2] * (360/(24*3600))

# Format Declination -> Angle [-90°; 90°]
def decToAngle (dec):
    return dec[0] + dec[1] * (1/60) + dec[2] * (1/3600)

# Format Angle 360 -> Radian
def angleToRad (angle):
    return (math.pi/180) * (angle-180)

# Format Angle -> Cartesian
def angleToCartesian (angle):
    rad=angleToRad(angle)
    return [math.cos(rad), math.sin(rad)]

# Modulo
def mod (a, n):
    while a < 0: a += n
    return a % n

#################
#   CONSTANTS   #
#################

# Standard Date Format ISO8601
ISO8601 = "%Y-%m-%dT%H:%M:%S.%f%z"

# Standard Epoch J2000.0 (base on UTC)
J2K = datetime.strptime('2000-01-01T11:58:55.816Z', ISO8601)

# Km / Light-Years
LY = 9.461 * (10 ** 12)

#############
# VARIABLES #
#############

# What time is it ?
now = datetime.utcnow()

# Earth Data
earth_xyz = [1,0,0] # 3D Location

# Sun Data (source: Wolfram Alpha)
sun_xyz = [0,0,0] # 3D Location
sun_ra = [11, 6, 39.5] # Right Ascension
sun_dec = [5, 42, 27.96] # Declination
sun_dist = 1.593 * (10 ** -5) # Light-Years

# Mars Data (s: WA)
mars_ra = [11, 1, 12.6]
mars_dec = [7, 25, 40.8]
mars_dist = 4.224 * (10 ** -5)

# Sirius Data (s: WA)
sirius_ra = [6, 45, 9.3]
sirius_dec = [-16, 42, 47.2]
sirius_dist = 8.597

########################
#   BASE CALCULATION   #
########################

# Earth-Sun
esu_vect = [
    earth_xyz[0] - sun_xyz[0],
    earth_xyz[1] - sun_xyz[1],
    earth_xyz[2] - sun_xyz[2]
]

metric = math.sqrt(
    (esu_vect[0] * sun_dist) ** 2 +
    (esu_vect[1] * sun_dist) ** 2 +
    (esu_vect[2] * sun_dist) ** 2
)

correct = [
    0 - raToAngle(sun_ra),
    0 - decToAngle(sun_dec)
]

print('1 unit = ' + str(metric))
print('Angular correction:', correct)

###################
#   CALCULATION   #
###################

def calcXYZ (ra, dec, dist):
    # Angles
    axy = mod(raToAngle(ra) + correct[0], 360)
    az = mod(decToAngle(dec) + correct[1] + 90, 180) - 90

    # Cartesian
    cxy = angleToCartesian(axy)
    cz = angleToCartesian(abs(az)+180)
    deep = cz[0] * dist

    # Position XYZ
    return [
        earth_xyz[0] + (cxy[0] * deep) / metric,
        earth_xyz[1] + (cxy[1] * deep) / metric,
        earth_xyz[2] + (cz[1] * dist) / metric
    ]

mars_xyz = calcXYZ(mars_ra, mars_dec, mars_dist)
sirius_xyz = calcXYZ(sirius_ra, sirius_dec, sirius_dist)

print('\nSun (x: %.3f, y: %.3f, z: %.3f)' % ( sun_xyz[0], sun_xyz[1], sun_xyz[2] ))
print('Earth (x: %.3f, y: %.3f, z: %.3f)' % ( earth_xyz[0], earth_xyz[1], earth_xyz[2] ))
print('Mars (x: %.3f, y: %.3f, z: %.3f)' % ( mars_xyz[0], mars_xyz[1], mars_xyz[2] ))
print('Sirius (x: %.3f, y: %.3f, z: %.3f)' % ( sirius_xyz[0], sirius_xyz[1], sirius_xyz[2] ))
