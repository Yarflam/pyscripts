import bpy
import random

# Blender v2.92.0 [updated]
# test command:
#   blender --background --python rain3D.py
# demo:
#   https://www.youtube.com/watch?v=BlIiCl9C-Co (26 August 2016 - v2.65)

class Rain3D:
    def __init__(self):
        self.timeline = 250
        self.drop = 500
        self.univ = [[-5,5], [-5,5], [0,15]]
        self.v = [0.1, 0.5]
        self.color = [0, 30, 255]
        self.new_scene()
    def idt(self, id):
        return '0' * (3-len(str(id))) + str(id)
    def randm(self, a, b):
        return random.random() * (b-a) + a
    def clear_scene(self):
        for elm in bpy.data.objects:
            elm.select_set(True if elm.type == 'MESH' else False)
        bpy.ops.object.delete()
    def new_scene(self):
        self.clear_scene()
        self.rain = []
        for cube in range(self.drop):
            bpy.ops.mesh.primitive_cube_add()
            if cube > 0:
                self.rain.append(bpy.data.objects['Cube.' + self.idt(cube)])
            else:
                self.rain.append(bpy.data.objects['Cube'])
            self.model_drop()
    def model_drop(self):
        slct = self.rain[len(self.rain)-1]
        for i in range(2):
            slct.data.vertices[i*4].co = (0.01, -0.01, i*0.5)
            slct.data.vertices[i*4+1].co = (0.01, 0.01, i*0.5)
            slct.data.vertices[i*4+2].co = (-0.01, 0.01, i*0.5)
            slct.data.vertices[i*4+3].co = (-0.01, -0.01, i*0.5)
        self.rand_location(slct)
        self.set_color(slct, self.color)
    def set_color(self, slct, c):
        mat = bpy.data.materials.new('rain')
        mat.diffuse_color = ((0.005*c[0]), (0.005*c[1]), (0.005*c[2]), 1.000)
        # mat.diffuse_shader = 'LAMBERT'
        # mat.diffuse_intensity = 1.0
        mat.specular_color = (1, 1, 1)
        # mat.specular_shader = 'COOKTORR'
        mat.specular_intensity = 0
        # mat.emit = 0
        if len(slct.data.materials) > 0:
            slct.data.materials[0] = mat
        else:
            slct.data.materials.append(mat)
    def rand_location(self, slct):
        slct.location.x = self.randm(self.univ[0][0], self.univ[0][1])
        slct.location.y = self.randm(self.univ[1][0], self.univ[1][1])
        slct.location.z = self.randm(self.univ[2][0], self.univ[2][1])
    def anim(self, scene, depsgraph):
        # frame = scene.frame.current
        for drop in self.rain:
            drop = scene.objects[drop.name_full]
            drop.location.z -= self.randm(self.v[0], self.v[1])
            if drop.location.z < 0:
                self.rand_location(drop)

if __name__ == "__main__":
    app = Rain3D()
    bpy.app.handlers.frame_change_post.clear()
    bpy.app.handlers.frame_change_post.append(app.anim)
    bpy.ops.screen.animation_play()
