#!/usr/bin/python3

class NetSvg:

	VERSION = '1.1'
	XMLS = 'http://www.w3.org/2000/svg'

	def __init__ (self, graph):
		self._graph = graph
		self._params = {
			'dx': 100, 'dy': 100,
			'mx': 0.5, 'my': 0.5,
			'radius': 1
		}

	def tag (self, name, props, close=0):
		isText = False
		tag = '<'+['','','?','!'][close % 4]+name
		for item in props:
			if item != 'text':
				tag += ' '+item+'="'+str(props[item])+'"'
			else: isText = True
		tag = tag+['','/','?',''][close % 4]+'>'
		return tag+props['text']+'</'+name+'>' if isText else tag

	def default (self, props, values):
		for item in values:
			if not item in props:
				props[item] = values[item]

	def export (self, path):
		radius = (min(self._params['dx'], self._params['dy'])/3) * self._params['radius']
		struct, links = self._graph.getStruct()
		chart = self.tag('svg', {'xmls': self.XMLS, 'version': self.VERSION,
			'height': self._params['dy'] * (len(struct) + self._params['my'])
		})
		# Draw the struct
		for y in range(0, len(struct)):
			for x in range(0, len(struct[y])):
				# Add a point
				chart = self.addCircle(chart, {
					'x': self._params['dx'] * (x + self._params['mx']),
					'y': self._params['dy'] * (y + self._params['my']),
					'radius': radius,
					'color': '#333'
				})
				# chart = self.addText(chart, {'x': 30*(x+1), 'y': 30*(y+1), 'text': struct[y][x].show() })
		# Draw the links
		for item in links:
			i, j, k = item
			chart = self.addLine(chart, {
				'A': [
					self._params['dx'] * (j + self._params['mx']),
					self._params['dy'] * (i + self._params['my'])
				],
				'B': [
					self._params['dx'] * (k + self._params['mx']),
					self._params['dy'] * (i+1 + self._params['my'])
				],
				'color': '#333',
				'width': radius/2
			})
		# Write the XML file
		file = open(path, 'wb')
		file.write((chart+'</svg>').encode())
		file.close()
		return True

	def addCircle (self, chart, props):
		self.default(props, { 'x': 0, 'y': 0, 'radius': 1, 'color': '#000' })
		return chart+self.tag('circle', {
			'cx': props['x'], 'cy': props['y'],
			'r': props['radius'],
			'fill': props['color']
		}, 1)

	def addLine (self, chart, props):
		self.default(props, { 'A': [0,0], 'B': [0,0], 'color': '#000', 'width': 1 })
		return chart+self.tag('line', {
			'x1': props['A'][0], 'y1': props['A'][1],
			'x2': props['B'][0], 'y2': props['B'][1],
			'stroke': props['color'],
			'stroke-width': props['width']
		}, 1)

	def addRect (self, chart, props):
		self.default(props, { 'width': 10, 'height': 10, 'x': 0, 'y': 0, 'color': '#000' })
		return chart+self.tag('rect', {
			'width': props['width'], 'height': props['height'],
			'x': props['x'], 'y': props['y'],
			'fill': props['color']
		}, 1)

	def addText (self, chart, props):
		self.default(props, { 'x': props['x'], 'y': props['y'], 'text': props['text'] })
		return chart+self.tag('text', {
			'x': props['x'], 'y': props['y'],
			'text': props['text']
		})
