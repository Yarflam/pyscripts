#!/usr/bin/python3
import math

def mtxRotX (t):
    return [
        [ 1, 0, 0 ],
        [ 0, math.cos(t), -math.sin(t) ],
        [ 0, math.sin(t), math.cos(t) ]
    ]

def mtxRotY (t):
    return [
        [ math.cos(t), 0, math.sin(t) ],
        [ 0, 1, 0 ],
        [ -math.sin(t), 0, math.cos(t) ]
    ]

def mtxRotZ (t):
    return [
        [ math.cos(t), -math.sin(t), 0 ],
        [ math.sin(t), math.cos(t), 0 ],
        [ 0, 0, 1 ]
    ]

def mtxProduct3x3 (a, b):
    next, output = range(0, 3), []
    for i in next:
        output.append([])
        for j in next:
            ij = 0
            for k in next:
                ij += a[i][k] * b[k][j]
            output[i].append(ij)
    return output

def mtxRotXYZ (x, y, z):
    return mtxProduct3x3(
        mtxRotZ(z), mtxProduct3x3(mtxRotX(x), mtxRotY(y))
    )

def toRadian (angle):
    return (angle-180) * (math.pi / 180)

def pt3D (point, camera, screen):
    # Translation
    pt = [
        point[0] - camera[0][0],
        point[1] - camera[0][1],
        point[2] - camera[0][2]
    ]
    # Rotation
    rot = mtxProduct3x3(
        [
            [ pt[0], 0, 0 ],
            [ 0, pt[1], 0 ],
            [ 0, 0, pt[2] ]
        ],
        mtxRotXYZ(
            toRadian(camera[1][0]),
            toRadian(camera[1][1]),
            toRadian(camera[1][2])
        )
    )
    pt = [
        rot[0][0] + rot[1][0] + rot[2][0],
        rot[0][1] + rot[1][1] + rot[2][1],
        rot[0][2] + rot[1][2] + rot[2][2],
    ]
    # Projection
    view = pt[2] + screen[3]
    view = screen[2] / ( view if view else 1 )
    return [
        pt[0]*view+screen[0]/2, pt[1]*-view+screen[1]/2,
        math.sqrt(pt[0]*pt[0]+pt[1]*pt[1]+pt[2]*pt[2])*(1 if pt[2]>=0 else -1)
    ]
