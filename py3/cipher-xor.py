#!/usr/bin/python3

def mask_xor(a, b):
    o = c = 0
    while a or b:
        o += 1 << c if not (a - (a >> 1 << 1)) == (b - (b >> 1 << 1)) else 0
        a, b = a >> 1, b >> 1
        c += 1
    return o


def cipher_enc(m, k):
    o = ""
    for i in range(len(m) + len(k)):
        x = ord(m[i % len(m)])
        e = (ord(k[i % len(k)]) * (i + 1)) % 256
        y = hex(mask_xor(x, e))[2:]
        o += "0" + y if len(y) < 2 else y
    return o


def cipher_dec(m, k):
    o = ""
    for i in range(int(len(m) / 2) - len(k)):
        y = int(m[i * 2:(i + 1) * 2], 16)
        e = (ord(k[i % len(k)]) * (i + 1)) % 256
        z = mask_xor(y, e)
        o += chr(z % 256)
    return o


def main():
    msg = str(input("msg>> "))
    key = str(input("key>> "))
    choice = str(input("Encrypt (y/n) ? "))
    if choice == "y":
        enc = cipher_enc(msg, key)
        print("\nENCRYPT\n" + enc)
    elif choice == "n":
        dec = cipher_dec(msg, key)
        print("\nDECRYPT\n" + dec)
    print("\n\n")

main()
