#!/usr/bin/python3
import math

# Curve function
def f(x, s):
    a, b, c = s
    return a * math.sin(b * x) * math.cos(c * x)

# Set the colors
black = chr(9617)
white = chr(9619)
grey = chr(9618)

# Settings
graph = {'Xmin': -0.2, 'Xmax': 1.2, 'Xsand': 100, 'Ysand': 2}
s = [5, 10, 6]

# Prepare
table = {}
for x in range(int(graph['Xmin'] * graph['Xsand']), int(graph['Xmax'] * graph['Xsand'])):
    # Calculate
    y = f(x / graph['Xsand'], s)
    # Give the pixel's position on Y
    z = int(math.floor(y * graph['Ysand']))
    # Create the row
    if not z in table:
        table[z] = black * int(x - graph['Xmin'] * graph['Xsand'])
    # Set the row
    for k in table:
        table[k] += black if not k == z else white

# Draw
for i in sorted(table.keys(), reverse=True):
    print(table[i])

# Output
print("\nFourier serie")
print("f(x) = %s*sin(%sx)*cos(%sx) for interval [%s;%s]\n" %
      (s[0], s[1], s[2], graph['Xmin'], graph['Xmax']))
