#!/usr/bin/python3
import time
import math
import os


def mtxRotX(t):
    return [
        [1, 0, 0],
        [0, math.cos(t), -math.sin(t)],
        [0, math.sin(t), math.cos(t)]
    ]


def mtxRotY(t):
    return [
        [math.cos(t), 0, math.sin(t)],
        [0, 1, 0],
        [-math.sin(t), 0, math.cos(t)]
    ]


def mtxRotZ(t):
    return [
        [math.cos(t), -math.sin(t), 0],
        [math.sin(t), math.cos(t), 0],
        [0, 0, 1]
    ]


def mtxProduct3x3(a, b):
    next, output = range(0, 3), []
    for i in next:
        output.append([])
        for j in next:
            ij = 0
            for k in next:
                ij += a[i][k] * b[k][j]
            output[i].append(ij)
    return output


def mtxRotXYZ(x, y, z):
    return mtxProduct3x3(
        mtxRotZ(z), mtxProduct3x3(mtxRotX(x), mtxRotY(y))
    )


def toRadian(angle):
    return (angle-180) * (math.pi / 180)


def pt3D(point, camera, screen):
    # Translation
    pt = [
        point[0] - camera[0][0],
        point[1] - camera[0][1],
        point[2] - camera[0][2]
    ]
    # Rotation
    rot = mtxProduct3x3(
        [
            [pt[0], 0, 0],
            [0, pt[1], 0],
            [0, 0, pt[2]]
        ],
        mtxRotXYZ(
            toRadian(camera[1][0]),
            toRadian(camera[1][1]),
            toRadian(camera[1][2])
        )
    )
    pt = [
        rot[0][0] + rot[1][0] + rot[2][0],
        rot[0][1] + rot[1][1] + rot[2][1],
        rot[0][2] + rot[1][2] + rot[2][2],
    ]
    # Projection
    view = pt[2] + screen[3]
    view = screen[2] / (view if view else 1)
    return [
        pt[0]*view+screen[0]/2, pt[1]*-view+screen[1]/2,
        math.sqrt(pt[0]*pt[0]+pt[1]*pt[1]+pt[2]*pt[2]) *
        (1 if pt[2] >= 0 else -1)
    ]


def renderShape(camera, screen, points, edges):
    out = []
    for point in points:
        pt = pt3D(point, camera, screen)
        out.append([pt[0], pt[1], '#.', pt[2]])
    for i in range(0, len(edges)):
        [start, end] = edges[i]
        out += renderLine(out[start], out[end])
    return out


def renderLine(start, end, fit=20):
    out = []
    if start[0] == end[0]:
        start = [start[1], start[0]]
        end = [end[1], end[0]]
        inv = True
    else:
        inv = False
    # Parameter A
    pA = end[0] - start[0]
    if not pA:
        pA = 1
    pA = (end[1] - start[1]) / pA
    # Parameter B
    pB = start[1] - start[0] * pA
    # Generate
    goal = (end[0]-start[0]) / fit
    for time in range(0, fit):
        x = start[0] + goal * time
        if not inv:
            out.append([x, x*pA+pB, '::'])
        else:
            out.append([x*pA+pB, x, '::'])
    return out


def renderScreen(screen, points):
    out = ''
    for y in range(screen[1]):
        for x in range(screen[0]):
            # Border
            if not y or y == screen[1]-1:
                if not x:
                    out += '@-'
                elif x == screen[0]-1:
                    out += '-@'
                else:
                    out += '--'
                continue
            elif not x:
                out += '| '
                continue
            elif x == screen[0]-1:
                out += ' |'
                continue
            # Cell
            cell = '  '
            for point in points:
                if round(point[1]) == y and round(point[0]) == x:
                    cell = point[2]
                    break
            out += cell
        out += '\n'
    print(out)


def moveCube(cube, offsets):
    out = []
    for point in cube:
        out.append([
            point[0] + offsets[0],
            point[1] + offsets[1],
            point[2] + offsets[2]
        ])
    return out


def rotateCube(cube, center, rotate):
    out = []
    for point in cube:
        rot = mtxProduct3x3(
            [
                [point[0]-center[0], 0, 0],
                [0, point[1]-center[1], 0],
                [0, 0, point[2]-center[2]]
            ],
            mtxRotXYZ(
                toRadian(rotate[0]),
                toRadian(rotate[1]),
                toRadian(rotate[2])
            )
        )
        out.append([
            rot[0][0] + rot[1][0] + rot[2][0] + center[0],
            rot[0][1] + rot[1][1] + rot[2][1] + center[1],
            rot[0][2] + rot[1][2] + rot[2][2] + center[2],
        ])
    return out


def main():
    cubeZ = 4
    # Parameters
    camera = [
        [4, 0, -3],
        [0, 0, 0]
    ]
    screen = [
        70, 30, 45, 1
    ]
    cube = [
        [-1, -1, cubeZ-1],
        [1, -1, cubeZ-1],
        [1,  1, cubeZ-1],
        [-1,  1, cubeZ-1],
        [-1, -1, cubeZ+1],
        [1, -1, cubeZ+1],
        [1,  1, cubeZ+1],
        [-1,  1, cubeZ+1]
    ]
    edges = [
        # Front
        [0, 1],
        [1, 2],
        [2, 3],
        [3, 0],
        # Back
        [4, 5],
        [5, 6],
        [6, 7],
        [7, 4],
        # Link
        [0, 4],
        [1, 5],
        [2, 6],
        [3, 7],
        # Cross
        # [0, 5],
        # [2, 7]
    ]
    # Render Shape
    while 1:
        points = renderShape(camera, screen, cube, edges)
        points = points + renderShape(
            camera, screen, moveCube(cube, [7, 1, 2]), edges)
        # os.system('clear')
        renderScreen(screen, points)
        time.sleep(0.01)
        cube = rotateCube(cube, [0, 0, cubeZ], [5, 0, 0])


main()
