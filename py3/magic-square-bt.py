#!/usr/bin/python3
import math

class Carre:
    def __init__(self, size):
        # Variable
        self.count = 0
        self.size = size
        self.table = [0] * (size**2)

    def verifLine(self, n):
        line = int(n / self.size)
        for i in range((n % self.size)):
            if self.table[line * self.size + i] == self.table[n]:
                return 0
        return 1

    def verifRow(self, n):
        row = (n % self.size)
        for i in range(int(n / self.size)):
            if self.table[i * self.size + row] == self.table[n]:
                return 0
        return 1

    def verifDiag(self, n):
        for i in range(int(n / self.size)):
            if (not n % (self.size + 1)):
                if self.table[(i * self.size + i)] == self.table[n]:
                    return 0
            elif (not n % (self.size - 1)):
                if self.table[(i * self.size + (self.size - i - 1))] == self.table[n]:
                    return 0
        return 1

    def backtrack(self, n):
        # Arret de la fonction
        if n == len(self.table):
            return 1
        # Attribution de la valeur
        for i in range(1, self.size + 1):
            self.table[n] = i
            self.count += 1
            if self.verifLine(n) and self.verifRow(n) and self.verifDiag(n):
                if(self.backtrack(n + 1)):
                    return 1
        self.table[n] = 0
        return 0

    def show(self):
        line = ""
        for i in range(len(self.table)):
            line += str(self.table[i])
            if (not ((i + 1) % self.size)):
                print(line)
                line = ""

def main ():
    app = Carre(6)
    app.backtrack(0) # calculer
    app.show() # afficher
    print("%s tests" % app.count)

main()
