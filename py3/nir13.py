#!/usr/bin/python

def testNir13 (key):
	return 1 if len(key) == 16 and (97 - (int(key[0:13]) % 97)) == int(key[14:]) else 0

def testProNir13 (key):
	if (len(key) == 16) and (key[13] == "-"):
		s, m, d, c = int(key[0]), int(key[3:5]), int(key[5:7]), int(key[14:]) # Unpacking
		test = 1 if s in [1,2] else 0
		test = test if m >= 1 and m <= 13 else 0
		test = test if (d >= 1 and d <= 95) or (d in [98,99]) else 0
		test = test if (97-(int(key[0:13])%97)) == c else 0
		return test
	return 0

print(testProNir13("0123456789012-34")) # Tester un numero de securite sociale
