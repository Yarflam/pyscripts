#!/usr/bin/python3
import tensorflow as tf
import math
import os

# Ignore warning
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# Results
W = tf.constant(0.)
X = tf.constant(0.)
Y = tf.constant(0.8)
Z = tf.constant(0.6)

# Parameters
a = tf.Variable(1.)
b = tf.Variable(1.)
c = tf.Variable(1.)
d = tf.Variable(1.)

# Links
sw = a * c
sx = b * c
sy = a * d
sz = b * d

# Constraints
fab = tf.sqrt(tf.square(a) + tf.square(b))
fab = tf.cond(tf.equal(fab, 0.), lambda: 1., lambda: 1./fab)

fcd = tf.sqrt(tf.square(c) + tf.square(d))
fcd = tf.cond(tf.equal(fcd, 0.), lambda: 1., lambda: 1./fcd)

ff = tf.abs(W-sw) + tf.abs(X-sx) + tf.abs(Y-sy) + tf.abs(Z-sz)
fg = tf.abs(tf.subtract(2.0, fab + fcd))

# Optimizing
equation = tf.train.AdamOptimizer(0.01).minimize(ff)
relation = tf.train.AdamOptimizer(0.01).minimize(fg)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    for i in range(500):
        sess.run(equation)
        sess.run(relation)

    print(sess.run([a,b,c,d]))
    print(sess.run([sw,sx,sy,sz]))
    print(sess.run([W,X,Y,Z]))
