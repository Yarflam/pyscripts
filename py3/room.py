#!/bin/python3

class Volume:
    def __init__(self):
        self._width = 0
        self._height = 0
        self._thick = 0
    def setWidth(self, value):
        self._width = value
        return self
    def setHeight(self, value):
        self._height = value
        return self
    def setThick(self, value):
        self._thick = value
        return self
    def getWidth(self): return self._width
    def getHeight(self): return self._height
    def getThick(self): return self._thick

class Location:
    def __init__(self):
        self._x = 0
        self._y = 0
        self._z = 0
    def setX(self, value):
        self._x = value
        return self
    def setY(self, value):
        self._y = value
        return self
    def setZ(self, value):
        self._z = value
        return self
    def getX(self): return self._x
    def getY(self): return self._y
    def getZ(self): return self._z

class Obj:
    def __init__(self):
        self.volume = Volume()
        self.location = Location()
    def getVolume(self):
        return self.volume
    def getLocation(self):
        return self.location

class ObjCtn(Obj):
    def __init__(self):
        super().__init__()
        self._inside = []
    def add(self, obj):
        self._inside.append(obj)

class Room(ObjCtn):
    def __init__(self, w, h):
        super().__init__()
        self.getVolume().setWidth(w).setHeight(h)

class Table(Obj):
    def __init__(self, w, h):
        super().__init__()
        self.getVolume().setWidth(w).setHeight(h)

def main ():
    room = Room(20, 20)
    table = Table(5,5)
    room.add(table)
    print(table.getVolume().getThick())

main()
