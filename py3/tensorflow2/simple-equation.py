#!/usr/bin/python3
import tensorflow as tf
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# Input
A = tf.Variable(1.)
B = tf.Variable(1.)
C = tf.Variable(1.)
D = tf.Variable(1.)
# Output
yAC = tf.constant(0.5)
yBC = tf.constant(0.5)
yAD = tf.constant(0.5)
yBD = tf.constant(0.5)

def calc():
    AC = A * C
    BC = B * C
    AD = A * D
    BD = B * D
    return [ AC, BC, AD, BD ]

def loss_1():
    AC, BC, AD, BD = calc()
    return tf.square(
        tf.abs(yAC-AC) ** 2 +
        tf.abs(yBC-BC) ** 2 +
        tf.abs(yAD-AD) ** 2 +
        tf.abs(yBD-BD)
    )

def loss_2():
    AC, BC, AD, BD = calc()
    fAB = tf.sqrt(tf.square(A) + tf.square(B))
    fAB = tf.cond(tf.equal(fAB, 0.), lambda: 1., lambda: 1./fAB)
    fCD = tf.sqrt(tf.square(C) + tf.square(D))
    fCD = tf.cond(tf.equal(fCD, 0.), lambda: 1., lambda: 1./fCD)
    return tf.abs(tf.subtract(2.0, fAB + fCD))

def train(loss, learning_rate):
    with tf.GradientTape() as t:
        current_loss = loss()
        dA, dB, dC, dD = t.gradient(current_loss, [A, B, C, D])
    A.assign_sub(learning_rate * dA)
    B.assign_sub(learning_rate * dB)
    C.assign_sub(learning_rate * dC)
    D.assign_sub(learning_rate * dD)

n_epoch = 5000
for epoch in range(n_epoch):
    if not (epoch % 500): print('Epoch %s - %s' % (epoch, epoch+500))
    train(loss_1, 0.01)
    train(loss_2, 0.01)

print('%s * %s ?= %s (-> %s)' % ( A.numpy(), C.numpy(), (A*C).numpy(), yAC.numpy() ))
print('%s * %s ?= %s (-> %s)' % ( B.numpy(), C.numpy(), (B*C).numpy(), yBC.numpy() ))
print('%s * %s ?= %s (-> %s)' % ( A.numpy(), D.numpy(), (A*D).numpy(), yAD.numpy() ))
print('%s * %s ?= %s (-> %s)' % ( B.numpy(), D.numpy(), (B*D).numpy(), yBD.numpy() ))

print('----')
print('A: %s' % ( A.numpy() ))
print('B: %s' % ( B.numpy() ))
print('C: %s' % ( C.numpy() ))
print('D: %s' % ( D.numpy() ))
