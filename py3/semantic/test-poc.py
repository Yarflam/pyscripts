#!/usr/bin/python3
import math

# Déclarer la classe Vector
class Vector:
    def __init__(self):
        self.features = []
    def fromDocs(self, docs):
        for i in range(0, len(docs)):
            print('> Fichier %s' % i)
            rows = docs[i].split('\n')
            for j in range(0, len(rows)):
                print(rows[j])
                rows[j] = self.embed(rows[j])
            docs[i] = rows
            print('[EOF]\n')
        return docs
    def embed(self, input):
        v = [0] * len(self.features)
        for i in range(0, len(input)):
            c = '¤' * i + input[i]
            # Save a feature
            if c not in self.features:
                self.features.append(c)
                v.append(1)
            else:
                v[self.features.index(c)] += 1
        return v
    def dist(self, a, b):
        maxLen = max(len(a), len(b))
        a += [0] * (maxLen - len(a))
        b += [0] * (maxLen - len(b))
        s = 0
        for i in range(0, maxLen):
            s += (b[i]-a[i]) * (b[i]-a[i])
        return [math.sqrt(s), a, b]

def main ():
    # Données
    a = '\n'.join(['ACCCGTGCTGCTGCTA', 'GATCGGG', 'ACGTTACCAGGC', 'ATGCA'])
    b = '\n'.join(['ACCGGAGTGCTGCAACCTA', 'CATTGCAACG', 'GATAACGGG', 'ATGCG'])

    # Vectoriser les données
    vect = Vector()
    docs = vect.fromDocs([ a, b ])

    # Comparer les lignes entre elles
    a, b = a.split('\n'), b.split('\n')
    minLenDocs = min(len(docs[0]), len(docs[1]))
    for i in range(0, minLenDocs):
        for j in range(0, minLenDocs):
            dist, vectA, vectB = vect.dist(docs[0][i], docs[1][j])
            if dist < 3:
                print('L%s -> L%s' % ((i+1), (j+1)))
                print('F1: ' + a[i])
                print('F2: ' + b[j])
                print('V1: <' + ','.join(str(x) for x in vectA)+'>')
                print('V2: <' + ','.join(str(x) for x in vectB)+'>')
                print('Dist: %s \n' % dist)

main()
