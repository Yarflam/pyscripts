#!/usr/bin/python3
from sklearn.feature_extraction.text import TfidfVectorizer

docs = []
with open('a.txt', 'r') as f: docs.append(f.read())
with open('b.txt', 'r') as f: docs.append(f.read())

kvect = TfidfVectorizer()
X = kvect.fit_transform(docs)

# Know the words
print(kvect.get_feature_names())
print('-'*50)

# Show the similarity
pairwise_similarity = X * X.T
print(pairwise_similarity.toarray())
print('-'*50)

# Show the matrix
print(X.toarray())
