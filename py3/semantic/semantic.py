#!/usr/bin/python3
from unicodedata import normalize, category
import math
import re


# Semantic space
class Semantic:
    def __init__(self):
        self._features = []
        self._maxLen = 0

    # Load some documents
    def loadDocs(self, docs):
        for i in range(0, len(docs)):
            docs[i] = self.cleanDoc(docs[i])
            for j in range(0, len(docs[i])):
                docs[i][j] = self.embedding(docs[i][j])
        return docs

    # Clean a document
    def cleanDoc(self, doc):
        doc = doc.lower()
        doc = ''.join((c for c in normalize(
            'NFD', doc) if category(c) != 'Mn'))
        doc = re.sub(r'((^|\n) +| +(\n|$))', r'\3',
                     re.sub('([^a-z0-9\n -]| +)', ' ', doc))
        return list(filter(None, doc.split('\n')))

    # Transform words to vector
    def embedding(self, words):
        vector = SmtcVector(self)
        words = words.split(' ')
        # For each word
        for i in range(0, len(words)):
            if not len(words[i]):
                continue
            # Add new feature
            if words[i] not in self._features:
                self._features.append(words[i])
            # Add the word
            vector.add(words[i])
        # Max Len (optional)
        self._maxLen = max(self._maxLen, len(words))
        return vector

    # Calculate the distance between two vectors
    def dist(self, a, b):
        a, b = a.getValue(), b.getValue()
        fit = (len(list(filter(lambda x: x > 0, a)))
               + len(list(filter(lambda x: x > 0, b)))) / 2
        # Pythagorean theorem
        sum = 0
        for i in range(0, self._maxLen):
            sum += (b[i]-a[i]) * (b[i]-a[i])
        return math.sqrt(sum) / fit

    # Return the current features
    def getFeatures(self):
        self._features.sort()
        self._features.sort(key=len)
        return self._features

    # Return the maximum length
    def getMaxLen(self):
        return self._maxLen


# Semantic vector
class SmtcVector:
    def __init__(self, semantic):
        self._semantic = semantic
        self._value = []

    # Add a new word
    def add(self, word):
        self._value.append(word)

    # Return the value
    def getValue(self):
        ft = self._semantic.getFeatures()
        maxFt = 1/len(ft)
        vect = list((ft.index(x) + 1) * maxFt for x in self._value)
        vect += [0.] * (self._semantic.getMaxLen() - len(vect))
        return vect

    # Return the words (not transformed)
    def getWords(self):
        return ' '.join(self._value)

    # Show the vector
    def __str__(self):
        return '<'+', '.join(str(x) for x in self.getValue())+'>'


# Example
def main():
    # Load the documents
    docs = []
    with open('a.txt', 'r') as f:
        docs.append(f.read())
    with open('b.txt', 'r') as f:
        docs.append(f.read())

    # Create a new semantic space
    app = Semantic()
    embeds = app.loadDocs(docs)
    print('Features:\n%s\n' % (', '.join(app.getFeatures())))

    # Evaluate the lines
    tolerance = 0.06
    for i in range(0, len(embeds[0])):
        for j in range(0, len(embeds[1])):
            d1, d2 = embeds[0][i], embeds[1][j]
            dist = app.dist(d1, d2)
            if dist < tolerance:
                print('L%s -> L%s' % ((i+1), (j+1)))
                print('D1: %s' % d1.getWords())
                print('D2: %s' % d2.getWords())
                print('V1: %s' % d1)
                print('V2: %s' % d2)
                print('Dist: %s' % dist)
                print(' ')


main()
