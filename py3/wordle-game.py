#!/bin/python
import unicodedata

def strip_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn')

def getWords(fname, nb):
    words = []
    with open(fname) as f:
        data = f.read().split('\n')
    for word in data:
        if not len(word) == nb: continue
        words.append(strip_accents(word).upper())
    return list(set(words))

def getFreq(words, nb):
    fq = [{} for _ in range(nb)]
    for word in words:
        for i in range(nb):
            if not word[i] in fq[i].keys():
                fq[i][word[i]] = 0
            fq[i][word[i]] += 1
    # for i in range(nb):
    #     fq[i] = {k:v for k, v in sorted(fq[i].items(),reverse=True,key=lambda item:item[1])}
    return fq

def getBestWord(words, fq):
    slcPt = 0
    slcWord = None
    nb = len(fq)
    for word in words:
        pt = 0
        for i in range(nb):
            pt += fq[i][word[i]] + (nb - word.count(word[i])) # Low repeat letter
            if not i or i == nb-1: pt += 1 # Priority: first & last letter
        if pt > slcPt:
            slcPt = pt
            slcWord = word
    return slcWord

def fWordle(words, ans, res):
    nb = len(ans)
    # Analyze -> build the filters
    filters = []
    wLetters = ''
    for i in range(nb):
        if res[i] == '0':
            filters.append(['nl', ans[i]]) # no letter
        if res[i] == '1':
            wLetters += ans[i]
            filters.append(['wl', ans[i]]) # with letter
            filters.append(['npl', ans[i], i]) # no place of letter
        if res[i] == '2':
            wLetters += ans[i]
            filters.append(['wl', ans[i]]) # with letter
            filters.append(['pl', ans[i], i]) # place of letter
    # Apply the filters
    out = []
    for word in words:
        err = 0
        for filter in filters:
            if filter[0] == 'nl' and not filter[1] in wLetters and filter[1] in word:
                err = filter
                break
            if filter[0] == 'wl' and not filter[1] in word:
                err = filter
                break
            if filter[0] == 'npl' and word[filter[2]] == filter[1]:
                err = filter
                break
            if filter[0] == 'pl' and not word[filter[2]] == filter[1]:
                err = filter
                break
        if err: continue
        out.append(word)
    return out

def main():
    nb = int(input('Nb letters: '))
    words = getWords('fr.dic', nb)
    for s in range(0, 6):
        # Best "word"
        freq = getFreq(words, nb)
        bestWord = getBestWord(words, freq)
        if not len(words):
            print('NO WORD')
            exit()
        print('# S%s -> %s' % (s+1, bestWord))
        # Ask
        ans, res = '', ''
        while not len(ans) == nb or ans not in words:
            ans = input('Word: ').upper()
            if ans == '#':
                finder = words.index(bestWord)
                words = words[0:finder] + words[finder+1:]
                bestWord = getBestWord(words, freq)
                print('# S%s -> %s' % (s+1, bestWord))
            if not len(ans): ans = bestWord
        while not len(res) == nb:
            res = input('Result: ').upper()
        # Filter
        words = fWordle(words, ans, res)

main()
