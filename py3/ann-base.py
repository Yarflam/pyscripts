#!/usr/bin/python3
# Artifical Neural Network - Base
from enum import Enum
import random
import math

class Activation(Enum):
    SIGMOID = 1
    TANH = 2
    GAUSS = 3
    HEAVISIDE = 4
    RELU = 5

#
#   Tools
#

def fctAct(x, act=Activation.SIGMOID):
    if act == Activation.SIGMOID:
        return 1 / (1 + math.exp(-x))
    if act == Activation.TANH:
        return 2 / (1 + math.exp(-2 * x)) - 1
    if act == Activation.GAUSS:
        return math.exp(-(x*x))
    if act == Activation.HEAVISIDE:
        return 1 if x >= 0 else 0
    if act == Activation.RELU:
        return x if x >= 0 else 0
    return x

#
#   Classes
#

class Neuron:
    def __init__ (self, w=[], b=0, act=Activation.SIGMOID):
        self.value = 0;
        self.w = w if type(w) == list else []
        self.b = b;
        self.act = act
    def build (self, rnd=False):
        if not rnd or not len(rnd) == 3: return self
        self.w = [random.uniform(rnd[0], rnd[1]) for i in range(rnd[2])]
        self.b = random.uniform(rnd[0], rnd[1])
        return self
    def predict (self, v):
        self.value = fctAct(sum([v[i] * self.w[i] for i in range(0, len(self.w))]) + self.b, self.act)
        return self.value
    def setValue (self, x):
        self.value = x
    def getValue (self):
        return self.value
    def getWeights (self):
        return self.w
    def getBias (self):
        return self.b

class Layer:
    def __init__ (self, neurons=[], prevLayer=False):
        self.neurons = neurons if type(neurons) == list else []
        self.prevLayer = prevLayer
    def build (self, nbNeurons=1, rnd=False, act=Activation.SIGMOID):
        nbPrevNeurons = len(self.prevLayer.neurons) if not self.prevLayer == False else 0
        rnd = [rnd[0], rnd[1], nbPrevNeurons] if type(rnd) == list and len(rnd) == 2 else False
        self.neurons = [Neuron([0] * nbPrevNeurons, 0, act).build(rnd) for i in range(0, nbNeurons)]
        return self
    def predict (self):
        if self.prevLayer == False: return []
        return [neuron.predict(self.prevLayer.getValues()) for neuron in self.neurons]
    def export (self):
        weights, bias = [], []
        for neuron in self.neurons:
            weights.extend(neuron.getWeights())
            bias.append(neuron.getBias())
        return [len(self.neurons), weights, bias]
    def setValues (self, x):
        if type(x) == list:
            for i in range(0, min(len(x), len(self.neurons))):
                self.neurons[i].setValue(x[i])
        else:
            for neuron in self.neurons: neuron.setValue(x)
        return self
    def getValues (self):
        return [neuron.getValue() for neuron in self.neurons]
    def getWeights (self):
        return [neuron.getWeights() for neuron in self.neurons]
    def getBias (self):
        return [neuron.getBias() for neuron in self.neurons]

class Network:
    def __init__ (self, map=[], rnd=False, act=Activation.SIGMOID):
        self.layers = [];
        self.build(map, rnd, act)
    def build (self, map=[], rnd=False, act=Activation.SIGMOID):
        if not type(map) == list: return
        # Input Layer (default=1)
        layer = Layer().build(map[0] if len(map) else 1, False, act)
        self.layers.append(layer)
        # Hidden Layer (default=0)
        for i in range(0, len(map) - 2 if len(map) > 2 else 0):
            layer = Layer([], layer).build(map[i+1], rnd, act)
            self.layers.append(layer)
        # Output Layer (default=1)
        outLayer = Layer([], layer).build(map[len(map)-1] if len(map) >= 2 else 1, rnd, act)
        self.layers.append(outLayer)
        return self
    def predict (self, x):
        if not len(self.layers) >= 2: return False
        values = self.layers[0].setValues(x)
        for i in range(1, len(self.layers)):
            values = self.layers[i].predict()
        return values
    def export (self):
        struct, weights, bias = [], [], []
        for i in range(0, len(self.layers)):
            stc, w, b = self.layers[i].export()
            struct.append(stc)
            weights.extend(w)
            bias.extend(b)
        return { 'struct': struct, 'weights': weights, 'bias': bias }
    def getValues (self):
        return [layer.getValues() for layer in self.layers]
    def getWeights (self):
        return [layer.getWeights() for layer in self.layers]
    def getBias (self):
        return [layer.getBias() for layer in self.layers]

#
#   Example - Learn Xor
#

def main():
    nbPop = 10 # population
    # Learning Set
    xor = { 'input': [[0,0], [0, 1], [1, 0], [1, 1]], 'output': [0, 1, 1, 0]}
    # Genetic Algorithm
    print('[LEARN]')
    best = { 'ann': False, 'err': 0 }
    for i in range(0, 200):
        # Create a population
        pop = [Network([2, 3, 1], [-1, 1], Activation.HEAVISIDE) for j in range(0, nbPop)]
        # Test
        for p in pop:
            err = sum([abs(xor['output'][k] - p.predict(xor['input'][k])[0]) for k in range(0, len(xor['input']))])
            if best['ann'] == False or err < best['err']:
                print('Index:', i, '; Score:', err)
                best['ann'] = p
                best['err'] = err
    # Result
    print('\n[RESULT]')
    print('Error:', best['err'])
    print('ANN:', best['ann'].export(),'\n')
    # Check
    print('[CHECKING]')
    for i in range(0, len(xor['input'])):
        print('x:', xor['input'][i], '; y:', xor['output'][i], '; z:', best['ann'].predict(xor['input'][i])[0])

main()
