#!/usr/bin/python3
from netsvg import NetSvg
import random

def dblBubbleSort (stack):
    imax = len(stack[0])
    for i in range(0, imax):
        test = i
        for j in range(i, imax):
            if stack[0][j] > stack[0][test]: test = j
        if test == i:
            stack[0][i], stack[0][test] = stack[0][test], stack[0][i]
            stack[1][i], stack[1][test] = stack[1][test], stack[1][i]
    return stack

class ObjNum:
    def __init__ (self, value=0):
        self.set(value)
    def set (self, value):
        self._value = value
    def get (self):
        return self._value
    def show (self):
        return self._value
    def count (self):
        return 1
    def add (self, b):
        if type(b) in [int, float]: b = ObjNum(b)
        return ObjNum(self._value + b.get())
    def sub (self, b):
        if type(b) in [int, float]: b = ObjNum(b)
        return ObjNum(self._value - b.get())
    def mul (self, b):
        if type(b) in [int, float]: b = ObjNum(b)
        return ObjNum(self._value * b.get())
    def div (self, b):
        if type(b) in [int, float]: b = ObjNum(b)
        return ObjNum(self._value / b.get())

class ObjTensor:
    def __init__ (self, value=[]):
        self._value = value
    def set (self, value):
        self._value = value
    def get (self):
        return self._value
    def show (self):
        return self._value
    def count (self):
        return len(self._value)
    def add (self, b):
        if type(b) in [ObjNum, ObjTensor]: b = b.get()
        return ObjTensor(self.calc('+', self._value, b))
    def sub (self, b):
        if type(b) in [ObjNum, ObjTensor]: b = b.get()
        return ObjTensor(self.calc('-', self._value, b))
    def mul (self, b):
        if type(b) in [ObjNum, ObjTensor]: b = b.get()
        return ObjTensor(self.calc('*', self._value, b))
    def dot (self, b):
        if type(b) in [ObjNum, ObjTensor]: b = b.get()
        return ObjTensor(self.calc('dot', self._value, b))
    def sum (self):
        # WORKING
        s = ObjNum(0)
        for x in self._value:
            s = s.add(x)
        return s
    def calc (self, ope, a, b):
        output = []
        if ope in ['+','-','*']:
            ##############
            # One matrix #
            ##############
            if type(a) in [int, float] or type(b) in [int, float]:
                a, b = [b, a] if type(a) in [int, float] else [a, b]
                a = [a] if not type(a) == list else a
                for i in range(len(a)):
                    output.append(self._calcNum(ope, a[i], b))
                return output
            ##############
            # Two matrix #
            ##############
            if not type(a) == list or not type(b) == list: return output
            if not len(a) or not len(b): return output
            # Dimension
            ax = len(a[0]) if type(a[0]) == list else 1
            bx = len(b[0]) if type(b[0]) == list else 1
            ay, by = len(a), len(b)
            # Calculate
            if (ax == bx) and (ay == by):
                for i in range(min(ay,by)):
                    output.append(self._calcNum(ope, a[i], b[i]))
            return output
        ###############
        # Dot Product #
        ###############
        if ope == 'dot':
            # In Working
            return output
        return output
    def _calcNum (self, ope, a, b):
        if ope == '+': return a + b
        if ope == '-': return a - b
        if ope == '*': return a * b
        return None

class ObjPlc:
    def __init__ (self, struct, id):
        self._id = id
        self._struct = struct
    def set (self, value):
        # WORKING
        if type(value) in [int, float]:
            self._value = ObjNum(value)
        elif type(value) == list:
            self._value = ObjTensor(value)
    def get (self):
        return self._value
    def getId (self):
        return self._id
    def show (self):
        return self._value.show()
    def count (self):
        return self.count()

class ObjVar:
    def __init__ (self, value):
        self.set(value)
    def set (self, value):
        if type(value) in [int, float]:
            self._value = ObjNum(value)
        elif type(value) == list:
            self._value = ObjTensor(value)
    def get (self):
        return self._value
    def show (self):
        return self._value.show()
    def count (self):
        return self.count()

class ObjConst:
    def __init__ (self, value):
        self.set(value)
    def get (self):
        return self._value
    def show (self):
        return self._value.show()
    def count (self):
        return self.count()

class ObjOpe:
    def __init__ (self, ope, a, b, id):
        self._id = id
        self._ope = ope
        self._a, self._b = a, b
        self._value = None
        self._reshape = None
    def get (self):
        return self._value
    def getId (self):
        return self._id
    def getName (self):
        return self._ope
    def show (self):
        return self._value.show()
    def count (self):
        return self._value.count()
    def setReshape (self, args):
        self._reshape = args
    def getArgs (self):
        return [self._a, self._b]
    def run (self):
        if self._ope == 'add':
            if type(self._b.get()) == ObjTensor: self._value = self._b.get().add(self._a.get())
            else: self._value = self._a.get().add(self._b.get())
        if self._ope == 'sub':
            if type(self._b.get()) == ObjTensor: self._value = self._b.get().sub(self._a.get())
            else: self._value = self._a.get().sub(self._b.get())
        if self._ope == 'mul':
            if type(self._b.get()) == ObjTensor: self._value = self._b.get().mul(self._a.get())
            else: self._value = self._a.get().mul(self._b.get())
        if self._ope == 'div':
            if type(self._b.get()) == ObjTensor: self._value = self._b.get().div(self._a.get())
            else: self._value = self._a.get().div(self._b.get())
        if self._ope == 'reshape': # easy test, don't keep it
            if self._reshape == [-1]:
                i, stack = 0, [self._a.get()]
                while i < len(stack):
                    if type(stack[i]) == ObjOpe:
                        stack[i] = stack[i].get()
                    if type(stack[i]) == ObjTensor:
                        stack[i] = stack[i].get()
                    if type(stack[i]) == list:
                        stack = stack[:i]+stack[i]+stack[i+1:]
                        i -= 1
                    i += 1
                self._value = ObjTensor(stack)

class ObjOpt:
    def __init__ (self, name, rate):
        self._name = name
        self._rate = rate
    def minimize (self, cost):
        self._cost = cost
        self._struct = self._getStruct()
        return self
    def show (self):
        return None
    def run (self):
        if self._name == 'gradientDescent':
            self._runGradientDescent()
    def _runGradientDescent (self):
        stackIndex, stackLayers = self._struct
        sCost = self._cost.get().sum().div(self._cost.count())
        # Each layers
        for i in range(0, len(stackLayers)):
            a, b = stackLayers[i].getArgs()
            # Detect the types
            test = 1 if type(a) == ObjVar and type(b) in [ObjOpe,ObjPlc] else 0
            test = 2 if type(b) == ObjVar and type(a) in [ObjOpe,ObjPlc] else test
            if test:
                # Reverse
                if test != 1: a,b = b,a
                # Linear Regression
                if stackLayers[i].getName() == 'add':
                    delta = ObjTensor([1]).mul(sCost).sum()
                    print('vx   :', 1)
                else:
                    delta = b.get().mul(sCost).sum().div(self._cost.count())
                    print('vx   :', b.show())
                # Update
                ratio = delta.mul(self._rate/self._cost.count())
                up = a.get().add(ratio)
                # Test
                print('delta:', delta.show())
                # print('before:', a.show())
                # print('after :', up.show())
                # Apply
                a.set(up.get())

    def _getStruct (self):
        i, stackLayers, stackIndex = 0, [self._cost], [self._cost.getId()]
        while i < len(stackLayers):
            a, b = stackLayers[i].getArgs()
            # Push on the stack
            if type(a) == ObjOpe and not a in stackLayers:
                stackLayers.append(a)
                stackIndex.append(a.getId())
            if type(b) == ObjOpe and not b in stackLayers:
                stackLayers.append(b)
                stackIndex.append(b.getId())
            # Intermediate Layer
            if type(a) == type(b) and type(a) == ObjOpe:
                stackLayers = stackLayers[:i]+stackLayers[i+1:]
                stackIndex = stackIndex[:i]+stackIndex[i+1:]
            else: i += 1
        # Apply a Bubble Sort and return the result
        return dblBubbleSort([stackIndex, stackLayers])

class Network:
    def __init__ (self):
        self._listPlc = [] # Placeholders
        self._listCst = [] # Constants
        self._listVar = [] # Variables
        self._listOpe = [] # Operations
        self._listOpt = [] # Optimizers
    def placeholder (self, struct):
        index = len(self._listPlc)
        self._listPlc.append(ObjPlc(struct, index))
        return self._listPlc[index]
    def const (self, value):
        self._listCst.append(ObjVar(value))
        return self._listCst[len(self._listCst)-1]
    def var (self, value):
        self._listVar.append(ObjVar(value))
        return self._listVar[len(self._listVar)-1]
    def add (self, a, b):
        index = len(self._listOpe)
        self._listOpe.append(ObjOpe('add', a, b, index))
        return self._listOpe[index]
    def sub (self, a, b):
        index = len(self._listOpe)
        self._listOpe.append(ObjOpe('sub', a, b, index))
        return self._listOpe[index]
    def mul (self, a, b):
        index = len(self._listOpe)
        self._listOpe.append(ObjOpe('mul', a, b, index))
        return self._listOpe[index]
    def div (self, a, b):
        index = len(self._listOpe)
        self._listOpe.append(ObjOpe('div', a, b, index))
        return self._listOpe[index]
    def reshape (self, a, args):
        obj = ObjOpe('reshape', a, None, len(self._listOpe))
        obj.setReshape(args)
        self._listOpe.append(obj)
        return obj
    def gradientDescent (self, rate):
        self._listOpt.append(ObjOpt('gradientDescent', rate))
        return self._listOpt[len(self._listOpt)-1]
    def run (self, callback):
        output = []
        for item in self._listOpe:
            item.run()
        for item in self._listOpt:
            item.run()
        for item in callback:
            output.append(item.show())
        return output
    def getStruct (self):
        if not len(self._listOpe): return []
        i, struct, links = 0, [[self._listOpe[len(self._listOpe)-1]]], []
        # Level
        while i < len(struct):
            # Layers
            for j in range(0, len(struct[i])):
                # Operation
                if type(struct[i][j]) == ObjOpe:
                    if i+1 >= len(struct): struct.append([])
                    # Get the children
                    a, b = struct[i][j].getArgs()
                    # Insert them in the list
                    if not a == None and not a in struct[i+1]:
                        links.append([i,j,len(struct[i+1])])
                        struct[i+1].append(a)
                    if not b == None and not b in struct[i+1]:
                        links.append([i,j,len(struct[i+1])])
                        struct[i+1].append(b)
            i += 1
        return [struct, links]

def f (x, a, b):
    output = []
    if not isinstance(x, list):
        return a * x + b
    for vx in x:
        output.append( f(vx, a, b) )
    return output

def genDatasetFromFct (a, b, n):
    features, labels = [], []
    for x in range(0, n):
        features.append(x)
        labels.append(f(x, a, b))
    return [features, labels]

def getPartOfDataset (dataset, start, dist):
    start %= len(dataset[0])
    end = (start+dist-1) % len(dataset[0]) + 1
    if start <= end:
        return [dataset[0][start:end], dataset[1][start:end]]
    return [
        dataset[0][start:]+dataset[0][:end],
        dataset[1][start:]+dataset[1][:end],
    ]

def Main ():
    # Dataset
    dataset = genDatasetFromFct(3.4, 7.8, 10)

    # Network
    net = Network()
    chart = NetSvg(net)
    x, y = net.placeholder([None]), net.placeholder([None])
    a, b = net.var(0), net.var(0)
    z = net.reshape(net.add(net.mul(a,x),b), [-1])

    # Gradient Descent
    loss = net.sub(z,y)
    cost = net.mul(loss,loss)
    optimizer = net.gradientDescent(0.01).minimize(cost)

    chart.export('./chart.html')

    # Training
    for train in range(0, 3):
        tx, ty = getPartOfDataset(dataset, (train % len(dataset[0])), 5)
        print("TX", tx)
        x.set(tx)
        y.set(ty)
        _ = net.run([a,x,b,y,z,loss,cost,optimizer])
        print('z: ', _[4], '\ny:', _[3])
        print('cost: ', cost.get().sum().show(), '\n')
    # print('a:',_[0],'\nx:',_[1],'\nb:',_[2],'\ny:',_[3],'\nz:',_[4],'\nloss:',_[5],'\ncost:',_[6],'\noptimizer:',_[7],'\n\n','-'*20,'\n')

Main()
