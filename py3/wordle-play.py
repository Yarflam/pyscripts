#!/bin/python
import unicodedata
import random

#
#   Playing
#

def strip_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn')

def getWords(fname, nb):
    words = []
    with open(fname) as f:
        data = f.read().split('\n')
    for word in data:
        if not len(word) == nb: continue
        words.append(strip_accents(word).upper())
    return list(set(words))

def getWordleDiff(word, goal):
    out = ''
    nb = len(word)
    for i in range(0, nb):
        if word[i] == goal[i]:
            out += '2'
        elif word[i] in goal:
            out += '1'
        else:
            out += '0'
    return out

def main():
    nb = int(input('Nb letters: '))
    words = getWords('fr.dic', nb)
    rnd = words[random.randint(0, len(words)-1)]
    for i in range(0, 6):
        print('Round %s' % (i+1))
        # Answer
        ans = ''
        while not len(ans) == nb or not ans in words:
            ans = input('> ').upper()
        # Analyze
        diff = getWordleDiff(ans, rnd)
        print(': %s' % diff)
        # Win?
        if not '0' in diff and not '1' in diff:
            print('Win!')
            break

main()
