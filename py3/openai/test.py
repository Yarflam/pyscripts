#!/usr/bin/python3
import configparser
import requests
import json
import os

# Config file
config = configparser.ConfigParser()
config.read('auth.ini') # Such like auth.example.ini
confOpenAI = config['OPENAI']

# Api Key
apikey = confOpenAI['KEY']

# Prompt Example
if not os.path.exists('prompt.tmp'):
    with open('prompt.tmp', 'w') as f:
        f.write('\n'.join([
            'Appliquer une opacité de 40%.',
            'opacity: 0.4;\n',
            'Afficher une ombre de 3px.',
            'box-shadow: 0 0 3px rgba(0,0,0,0.5);\n',
            'Définir une couleur orange au texte.',
            'color: orange;\n',
            'Appliquer une marge de 20px.\n'
        ]))

#
# Docs
# Engines: ada, babbage, curie, davinci
# +: content-filter-alpha-c4, content-filter-dev, cursing-filter-v6
# +: curie-instruct-beta, davinci-instruct-beta
#

# Get the data
with open('prompt.tmp', 'r') as f:
    prompt = f.read()

# Completion
engine = 'curie'
payload = {
    'prompt': prompt,
    'max_tokens': 80,
    'temperature': 1,
    'top_p': 1,
    'n': 1,
    'stream': False,
    'logprobs': None,
    'stop': '\n\n',
    'presence_penalty': 0,
    'frequency_penalty': 0,
    'best_of': 1
    # 'logit_bias': None
}

# Execute
headers = {
    'content-type': 'application/json',
    'Authorization': 'Bearer %s' % apikey
}
data = json.dumps(payload)
url = 'https://api.openai.com/v1/engines/%s/completions' % engine
out = requests.post(url, headers=headers, data=data)
answer = json.loads(out.text)

# Answer
print('%s%s' % ( prompt, answer['choices'][0]['text'] ))
