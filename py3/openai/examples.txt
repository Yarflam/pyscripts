3 initial sentences + one AI answer

[[ 4 translations ]]

fr: Je cours dans la rue.
en: I run in the street.
es: Corro por la calle.
de: Ich laufe die Straße entlang.

fr: Tu chantes sous la fenêtre.
en: You sing under the window.
es: Canta bajo la ventana.
de: Sie singen unter dem Fenster.

fr: Le bateau a coulé en heurtant un récif.
en: The boat sank when it hit a reef.
es: El barco se hundió al chocar con un arrecife.
de: Das Boot sank, als es auf ein Riff traf.

fr: La fleur est tombé du balcon et Marion l'a rattrapé.
en: The flower fell from the balcony and Marion caught it.
es: La flor cayó del balcón y Marion la cogió.
de: Die Blume fiel vom Fensterbrett und Marion fing sie auf,
